# Airport-Mediation-complaint

Greasemonkey script [Airport-Mediation-complaint.js](https://bitbucket.org/OPiMedia/airport-mediation-complaint/raw/master/Airport-Mediation-complaint.js)
to add three buttons to the form of [Airport Mediation](http://www.airportmediation.be/) (Belgium)
to send a [complaint](https://www.cognitoforms.com/FodMobiliteit1/AIRPORTMEDIATIONFR)
about plane ✈✈✈ nuisances.
Each of these buttons prefill the form for a kind of complaint. 😱

![buttons](http://www.opimedia.be/survol-de-Bruxelles/_png/Airport-Mediation-complaint--3-buttons.png)

Install [Greasemonkey](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/) plugin to Firefox.
(Or maybe [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) plugin to Chrome. Not tested).

Install this script to Greasemonkey (or Tampermonkey), and **important**,
complete the data constants with correct values for you (name, address, etc.).

🔴 Si subitement ce script ne fonctionne plus
c'est que le service censé récolter vos plaintes a changé le formulaire.
Dès que je m'en rend compte je mets le script à jour.
Il vous suffit de recopier-coller le nouveau contenu du fichier comme expliqué ci-dessous.

Sachez que le service censé récolter vos plaintes
fait des pieds et des mains pour réduire artificiellement le nombre de plaintes !



## Installation étape par étape (en français)

1. Utilisez Firefox :
   <https://www.mozilla.org/fr/firefox/>
2. Avec Firefox installez le module Greasemonkey :
   <https://addons.mozilla.org/fr/firefox/addon/greasemonkey/>
3. Une fois installé une petite tête de singe
   ![Greasemonkey](https://addons.cdn.mozilla.net/user-media/addon_icons/0/748-64.png)
   apparaît dans les icônes au-dessus des pages Web.
   Cliquez dessus et choisissez "New user script...".
4. Une fenêtre s'ouvre.
   Effacez les quelques lignes qu'elle contient,
   et copiez-collez dans cette fenêtre le contenu de ce fichier :
   <https://bitbucket.org/OPiMedia/airport-mediation-complaint/raw/master/Airport-Mediation-complaint.js>
5. Remplacez tous les "???" par vos coordonnées.
   Attention à bien conserver les guillemets et à ne rien changer d'autres.
   (Sauf si vous savez ce que vous faites,
   mais sachez que la moindre virgule au mauvais endroit en dehors de ces "???"
   peut empêcher le script de fonctionner.
   N'ajouter pas non plus de guillemet " à l'intérieur des guillemets.
   Néanmoins rien de grave ne peut arriver.
   Si ça ne fonctionne plus recopiez-collez simplement le contenu du fichier.)
   Vous pouvez aussi ajouter une description et une question entre les guillemets "".
   Cela remplira les champs "Description des faits" et "Formulez votre question…"
   mais ceux-ci ne seront réellement pris en compte pour l'envoi que si vous cliquez dedans
   et en validez le contenu en le modifiant (par exemple en y ajoutant une ligne vide à la fin).

6. Maintenant chaque fois que vous irez sur le formulaire avec ce lien direct
   vous aurez trois nouveaux boutons en haut de la page à gauche,
   et l'heure affichée sur la droite :
   <https://www.cognitoforms.com/FodMobiliteit1/AIRPORTMEDIATIONFR>
   Cliquez sur le premier ou le deuxième bouton rempli les champs avec vos coordonnées
   et sélectionne une plainte pour un décollage.
   Au besoin changez les informations (notamment la date et d'heure)
   puis quand tout est prêt vous pouvez cliquer sur "Envoyer".
   Le troisième bouton fait la même chose
   mais sélectionne le jour complet avec la date de la veille.

7. Le temps que vous envoyez la plainte un nouvel avion ✈ arrive, retournez simplement au point 6.

Une fois le script enregistré dans Greasemonkey il reste
modifiable en cliquant sur la petite tête de singe,
sélectionnant le script puis en cliquant sur "Edit".
Vous pouvez saisir vos coordonnées et essayer sur la page de plainte sans problème.
Tant que vous ne cliquez pas sur "Envoyer" rien ne se passe.



## Related links

- ✈✈✈ survol de Bruxelles ✈✈✈:
  <http://www.opimedia.be/survol-de-Bruxelles/>
- *Brussels Airport: il sera désormais plus facile d'introduire une requête auprès du service de médiation* (RTBF Info, 20 février 2019):
  <https://www.rtbf.be/info/belgique/detail_aeroport-de-bruxelles-national-il-sera-desormais-plus-facile-d-introduire-une-requete-aupres-du-service-de-mediation?id=10150776>
  “Le but n’est pas de réduire le nombre de plaintes mais bien de clarifier la procédure.”

    - <https://twitter.com/RTBFinfo/status/1098255462414340097>



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬

🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

  - 📧 <olivier.pirson.opi@gmail.com>
  - Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
  - 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
  - other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)

Copyright (C) 2019, 2020 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.



![complaint](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Feb/18/3546263887-1-airport-mediation-complaint-logo_avatar.png)
