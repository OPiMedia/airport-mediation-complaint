// ==UserScript==
// @name          Airport-Mediation-complaint
// @namespace     http://www.opimedia.be/survol-de-Bruxelles/
// @description   Add three buttons to auto-complete complaint form.
// @include       https://www.cognitoforms.com/FodMobiliteit1/AIRPORTMEDIATIONFR
// @require       https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js
// @grant         none
// ==/UserScript==

/* jshint esversion: 6 */

/*
 * Airport Mediation complaint (November 2, 2019)
 *
 * Greasemonkey script to add three buttons to the form of Airport Mediation (Belgium) to send a complaint about plane ✈✈✈ nuisances:
 * https://www.cognitoforms.com/FodMobiliteit1/AIRPORTMEDIATIONFR
 * Each of these buttons prefill the form for a kind of complaint.
 *
 * Install Greasemonkey plugin to Firefox:
 * https://addons.mozilla.org/fr/firefox/addon/greasemonkey/
 *
 * Or Tampermonkey plugin to Chrome (not tested):
 * https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo
 *
 * Install this script to Greasemonkey (or Tampermonkey), and IMPORTANT,
 * complete the data constants below with correct values for you (name, address, etc.).
 *
 * GPLv3 --- Copyright (C) 2019 Olivier Pirson --- http://www.gnu.org/licenses/gpl.html
 * Olivier Pirson --- http://www.opimedia.be/
 */

(function () {
    "use strict";

    /* Mandatory data constants (replace ??? by your personal information) */
    const gender = "H";  // "H", "F", or "X"
    const firstname = "???";
    const surname = "???";
    const address = "???";
    const addressNumber = "???";
    const city = "???";
    const zip = "???";
    const phone = "";  // optional
    const mobile = "";  // optional
    const email = "???";

    const complaintDay = "???";  // "Formulez votre réclamation" for day button
    const complaintNight = "???";  // "Formulez votre réclamation" for night button
    const complaintGeneral = "???";  // "Formulez votre réclamation" for general button

    /* End of data constants */



    /* Code of each field used */
    const codeGender = "c-0-65";  // "Sexe"
    const codeFirstname = "c-1-64";
    const codeSurname = "c-2-64";
    const codeAddress = "c-4-63";
    const codeAddressNumber = "c-5-62";
    const codeCity = "c-6-61";
    const codeZip = "c-7-60";
    const codePhone = "c-8-59";
    const codeMobile = "c-9-58";
    const codeEmail = "c-10-57";
    const codeDate = "c-16-45";
    const codeTime = "c-17-44";

    const codeTypeOneFlight = "c-12-52"; // "Contenu de la demande" - "Réclamation spécifique à un survol précis"
    const codeTypeGeneral = "c-12-53"; // "Contenu de la demande" - "Réclamation générale sur les conditions de survols"

    const codePeriod = "c-18-43"; // "Période des faits" Jour : 06h00 à 22h59
    const codePeriodAllDay = "Jour : 06h00 à 22h59";

    const codeOneFlightLanding = "c-19-41";  // "Atterrissage/décollage" - "Procédure d'atterisage"
    const codeOneFlightTakeOff = "c-19-42";  // "Atterrissage/décollage" - "procédure de décollage"

    const codeGeneralTakeOff = "c-22-28";  // "Votre réclamation concerne" - "procédure de décollage"

    const codeAnswerYes = "c-25-10";  // "Souhaitez-vous une réponse?" - "Oui"

    const codeListNo = "c-26-8";  // "Souhaitez-vous recevoir chaque semaine la list explicative des déviations ainsi que nos autres publications?" - "Non"

    const codeComplaint = "c-24-12";  // "Formulez votre réclamation"

    const codeSubmit = "c-submit-button";  // "Envoyer"



    /**
     * Set the radio button id.
     *
     * @param id string
     */
    function checkRadio(id) {
        console.log("checkRadio", id);

        const item = document.getElementById(id);

        if (item) {
            item.parentNode.style.color = "green";
            item.checked = true;
            $(item).click();
        }
    }


    /**
     * Set the option in select button id.
     *
     * @param id string
     * @param option string
     */
    function checkSelect(id, option) {
        console.log("checkSelect", id, option);

        const item = document.getElementById(id);

        if (item) {
            item.style.color = "green";
            Array.from(item.children).forEach(function (item, v) {
                if (item.value === option) {
                    item.selected = true;
                    $(item).click();
                }
            });
        }
    }


    /**
     * Apply func(key, value) for each key: value from dict.
     *
     * @param dict Associtative table
     * @param func Function
     */
    function dictForEach(dict, func) {
        for (let key in dict) {
            func(key, dict[key]);
        }
    }


    /**
     * Finish the preparation of the form.
     */
    function end() {
        console.log("====================");
        document.getElementById(codeComplaint).style.height = "18ex";

        window.setTimeout(function () {
            $("body").click();
            $("body").focus();
            $("body").blur();

            document.getElementById(codeSubmit).focus();
            window.scrollTo(0, document.body.scrollHeight);
            console.log("READY to send");
        }, 500);
    }


    /**
     * Return (in a string) the current time.
     */
    function getTime() {
        const now = new Date();

        return now.getHours() + ":" + now.getMinutes();
    }


    /**
     * Return (in a string) the current date.
     */
    function getToday() {
        const now = new Date();

        return padding2(now.getDate()) + "-" + padding2(now.getMonth() + 1) + "-" + now.getFullYear();
    }


    /**
     * Return (in a string) the date of yesterday.
     */
    function getYesterday() {
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);

        return padding2(yesterday.getDate()) + "-" + padding2(yesterday.getMonth() + 1) + "-" + yesterday.getFullYear();
    }


    /**
     * Insert text in beginning of each tag <tagName>.
     *
     * @param tagName string
     * @param text string
     */
    function insertBeginTagName(tagName, text) {
        console.log("insertBeginTagName", tagName, text);

        const items = document.getElementsByTagName(tagName);

        for (let i = 0; i < items.length; ++i) {
            const item = items[i];

            item.innerHTML = text + item.innerHTML;
        }
    }


    /**
     * Set the element id with value.
     *
     * @param id string
     * @param value string
     */
    function idSetValue(id, value) {
        console.log("idSetValue", id, value);

        const item = document.getElementById(id);

        if (item) {
            item.style.color = "green";
            item.value = value;
            $(item).click();
            item.focus();
            item.blur();
            if (item.tagName === "TEXTAREA") {
                const div = document.createElement("div");

                div.innerHTML = "Cette zone de saisie doit être validée manuellement. (Ajoutez une ligne vide.)";
                div.style.backgroundColor = "orange";
                item.parentNode.appendChild(div);
            }
        }
    }


    /**
     * Return n converting to a string with a extra left 0 to have at least 2 characters.
     *
     * @param number
     */
    function padding2(n) {
        return (n < 10 ? "0" + n.toString() : n.toString());
    }


    /**
     * Set all data for the complete yesterday during the day.
     */
    function setCompleteYesterdayDay() {
        console.log("=== setCompleteYesterdayDay ===");

        checkSelect(codeGender, gender);

        checkRadio(codeTypeGeneral);
        checkRadio(codeGeneralTakeOff);
        checkRadio(codeAnswerYes);
        checkRadio(codeListNo);

        checkSelect(codePeriod, codePeriodAllDay);

        dictForEach({[codeFirstname]: firstname,
                     [codeSurname]: surname,
                     [codeAddress]: address,
                     [codeAddressNumber]: addressNumber,
                     [codeCity]: city,
                     [codeZip]: zip,
                     [codePhone]: phone,
                     [codeMobile]: mobile,
                     [codeEmail]: email,
                     [codeDate]: getYesterday(),
                     [codeComplaint]: complaintGeneral},
                    idSetValue);

        end();
    }


    /**
     * Set all data for a flight during the day.
     */
    function setFlightDay() {
        console.log("=== setFlightDay ===");

        checkSelect(codeGender, gender);

        checkRadio(codeTypeOneFlight);
        checkRadio(codeOneFlightTakeOff);
        checkRadio(codeAnswerYes);
        checkRadio(codeListNo);

        dictForEach({[codeFirstname]: firstname,
                     [codeSurname]: surname,
                     [codeAddress]: address,
                     [codeAddressNumber]: addressNumber,
                     [codeCity]: city,
                     [codeZip]: zip,
                     [codePhone]: phone,
                     [codeMobile]: mobile,
                     [codeEmail]: email,
                     [codeDate]: getToday(),
                     [codeTime]: getTime(),
                     [codeComplaint]: complaintDay},
                    idSetValue);

        end();
    }


    /**
     * Set all data for a flight during the night.
     */
    function setFlightNight() {
        console.log("=== setFlightNight ===");

        checkSelect(codeGender, gender);

        checkRadio(codeTypeOneFlight);
        checkRadio(codeOneFlightTakeOff);
        checkRadio(codeAnswerYes);
        checkRadio(codeListNo);

        dictForEach({[codeFirstname]: firstname,
                     [codeSurname]: surname,
                     [codeAddress]: address,
                     [codeAddressNumber]: addressNumber,
                     [codeCity]: city,
                     [codeZip]: zip,
                     [codePhone]: phone,
                     [codeMobile]: mobile,
                     [codeEmail]: email,
                     [codeDate]: getToday(),
                     [codeTime]: getTime(),
                     [codeComplaint]: complaintNight},
                    idSetValue);

        end();
    }


    /**
     * Update the show-time zone.
     */
    function showTime() {
        const d = new Date();
        const hours = d.getHours();
        const minutes = d.getMinutes();
        const seconds = d.getSeconds();
        const contenair = document.getElementById("show-time");

        contenair.innerHTML = hours + "h" + minutes + "&prime;" + seconds;
    }



    /*
     * Main
     */
    /* Insert items in the page */
    insertBeginTagName("body",
                       "<div style='background-color:orange; padding:1ex'>" +
                       "  <button id='button-flight-day'>Survol JOUR</button>" +
                       "  <button id='button-flight-night'>Survol NUIT</button>" +
                       "  <button id='button-complete-yesterday-day'>HIER journée complète</button>" +
                       "  <small>(version du 2 novembre 2019)</small>" +
                       "  <div id='show-time' style='background-color:orange; font-family:monospace; font-size:x-large ; font-weight:bold; position:fixed; padding:1ex; right:0; top:0'></div>" +
                       "  <div>" +
                       "    Boutons et heure ajoutés par le script pour aider au remplissage du formulaire. Si cela ne fonctionne plus mettez le script à jour : <a href='https://bitbucket.org/OPiMedia/airport-mediation-complaint/'>https://bitbucket.org/OPiMedia/airport-mediation-complaint/</a>" +
                       "  </div>" +
                       "</div>");


    document.getElementById("button-complete-yesterday-day").addEventListener("click", setCompleteYesterdayDay, false);
    document.getElementById("button-flight-night").addEventListener("click", setFlightNight, false);
    document.getElementById("button-flight-day").addEventListener("click", setFlightDay, false);

    /* Run the time zone */
    showTime();
    window.setInterval(showTime, 500);

    /* Move to the top of the page */
    window.scrollTo(0, 0);
}());
